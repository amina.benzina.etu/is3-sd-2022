#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "liste_point.h"

#define N 10
#define SCENARIO 73738

/*
 * Trie le tableau pnts par angle polaire croissant
 * Retourne true si les points ont des angles polaires distincts deux-à-deux
 * Retourn false sinon
 * nb_pnts = le nombre de points
 */

bool
trie_points (int nb_pnts, struct point *pnts)
{
  qsort (pnts, nb_pnts, sizeof (struct point), &compare_points);
  for (int i = 0; i < nb_pnts - 1; i++)
    if (compare_points (&pnts[i], &pnts[i + 1]) == 0)
      return false;
  return true;
}

/*
 * Affecte à envlp l'enveloppe convexe de pnts (le premier point
 *      et le dernier sont identiques).
 * Affecte nb_envlp le nombre de points de cette enveloppe
 * En entrée, pnts est un tableau de nb_pnts points, triés par
 *      angle polaire croissant. 
 */

void
Graham (int *nb_envlp, struct point *envlp, int nb_pnts, struct point *pnts)
{
  struct liste_point L;
/*
 * A FAIRE : 
 * 1. Programmer l'algorithme. Utiliser la liste de points L pour la pile
 * 2. Enregistrer les points de la pile dans le tableau envlp
 *      et le nombre de ses éléments dans nb_envlp
 */
}

int
main ()
{
  struct point T[N], E[N + 1];
  FILE *f;
  int i, nb_E;

  srand48 (SCENARIO);
/* On crée N points qu'on enregistre dans "points.dat" (A est en (0,0)). */
  init_point (&T[0], 0, 0, 'A');
  for (i = 1; i < N; i++)
    {
      double x, y;
      x = drand48 ();
      y = drand48 ();
      init_point (&T[i], x, y, 'A' + i);
    }
  f = fopen ("points.dat", "w");
  assert (f != NULL);
  for (i = 0; i < N; i++)
    fprintf (f, "%f %f %c\n", T[i].x, T[i].y, T[i].ident);
  fclose (f);
/* Vérification */
  if (!trie_points (N - 1, T + 1))
    {
      fprintf (stderr, "alignement de points non traité\n");
      exit (1);
    }
/* Affichage pour debugger */
  printf ("par angle polaire croissant :");
  for (i = 0; i < N; i++)
    printf (" %c", T[i].ident);
  printf ("\n");
/* Application de l'algo de Graham. Résultat dans nb_E et E */
  Graham (&nb_E, E, N, T);
/* Enregistrement des points de E dans "enveloppe.dat" */
  f = fopen ("enveloppe.dat", "w");
  assert (f != NULL);
  for (i = 0; i < nb_E; i++)
    fprintf (f, "%f %f\n", E[i].x, E[i].y);
  fclose (f);
  return 0;
}

