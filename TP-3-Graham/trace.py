#! /usr/bin/python3

import numpy as np
import matplotlib.pyplot as plt
points = np.loadtxt('points.dat', dtype="f, f, U1")
enveloppe = np.loadtxt('enveloppe.dat')
for p in points :
    plt.scatter (p[0], p[1])
    plt.annotate (p[2], (p[0]+5e-3, p[1]+5e-3))

plt.plot (enveloppe[:,0], enveloppe[:,1])
plt.show ()

