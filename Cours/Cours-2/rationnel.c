/* rationnel.c */

#include <stdlib.h> /* pour malloc et free */
#include <stdio.h> /* pour printf */
#include <assert.h> /* pour assert */
#include "rationnel.h"

/* retourne le pgcd de a0 et b0 */

static int Euclide (int a0, int b0)
{   int a, b, t;

    if (a0 > 0) a = a0; else a = - a0;
    if (b0 > 0) b = b0; else b = - b0;
    while (b != 0)
    {   t = a % b;   /* le reste de la division de a par b */
        a = b;
        b = t;
    }
    return a;
}

/* Affectation sur A, supposé initialisé */

void set_rationnel (struct rationnel* A, int n, int d)
{   int g;

    assert (d != 0);

    g = Euclide (n, d);
    A->data[0] = n / g;
    A->data[1] = d / g;
    if (A->data[1] < 0)
    {   A->data[0] = - A->data[0];
        A->data[1] = - A->data[1];
    }
}

/* Constructeur. 
   Comme A est supposée non initialisée, il faut faire le malloc.
   Initialise A avec la fraction n/d.
   Le dénominateur d est non nul.
 */

void init_rationnel (struct rationnel* A, int n, int d)
{  
    A->data = (int*)malloc (2 * sizeof(int));
    set_rationnel (A, n, d);
}

/* Destructeur.
   La variable A est sur le point de disparaître 
        (déplacement de SP dans la pile) 
   C'est le dernier moment où on peut libérer ce qui est immobilisé par A
 */

void clear_rationnel (struct rationnel* A)
{
    free (A->data);
}

/* Affecte à S (mode R) le rationnel A + B (A, B mode D) */
void add_rationnel
            (struct rationnel* S, struct rationnel* A, struct rationnel* B)
{
    int n, d;
    n = A->data[0] * B->data[1] + A->data[1] * B->data[0];
    d = A->data[1] * B->data[1];
/* 
   Surtout ne pas appliquer le constructeur sur S qui est déjà initialisée
   sinon fuite mémoire 
    init_rationnel (S, n, d);
 */
    set_rationnel (S, n, d);
}

void print_rationnel (struct rationnel* A)
{
    if (A->data[1] == 1)
        printf ("%d\n", A->data[0]);
    else
        printf ("%d/%d\n", A->data[0], A->data[1]);
}

